import React, {Component, useState} from 'react';
import beers, {styles, countries} from "./db.js"
import {Container, Row, Col, Button, Modal, DropdownButton, Dropdown} from 'react-bootstrap'

export default function BeerList()  {

    const [show, setShow] = useState(false);
    const handleClose = () => setShow(false);
    const handleShow = () => setShow(true);

    const countryFilter = countries.map(country=> 
        <Dropdown.Item> {country.name} </Dropdown.Item>
    );
    
    const styleFilter = beers.styles.map(style=> 
        <Dropdown.Item> {style.name} </Dropdown.Item>
    );

    const beerList = beers.beers.map(beer=> 
        <Col xs={4}>
            <Button onClick={handleShow} variant="light"><img src={beer.image_path} alt="my image"/></Button>

            <Modal show={show} onHide={handleClose} centered={true} size='lg'>
                <Modal.Header closeButton>
                <Modal.Title>{beer.title}</Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <Container>
                        <Row>
                            <Col>
                                <img src={beer.image_path} alt="my image"/>
                            </Col>
                            <Col>
                                <Row>
                                <h2>Name: {beer.title} </h2>
                                <h2>Style: {styles[beer.style_id - 1].name} </h2>
                                <h2>Country: {countries[beer.country_id - 1].name} </h2>
                                </Row>
                                
                                <Row>
                                <p>{beer.description}</p>
                                </Row>
                            </Col>
                        </Row>
                    </Container>
                </Modal.Body>
            </Modal>

            <h1> {beer.title} </h1>
            
            <Button variant="danger"> Favorite </Button>
        </Col>
    );

    return (
        <Container fluid="lg">
            <DropdownButton id="country-filter" title="Filter by Country">
                    {countryFilter}
            </DropdownButton>

            <DropdownButton id="style-filter" title="Filter by Style">
                {styleFilter}
            </DropdownButton>
            
            <Row>
                {beerList}
            </Row> 
        </Container>
    )
}

