import React from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import BeerList from './BeerList'
import Container from 'react-bootstrap/Container'

 export default function App() {

    return (
        <div id="App">
          <Container fluid ="lg">
            <h1>Beerify</h1>
            <h2>List of beers</h2>
          </Container>
          <BeerList/>
        </div>
    )


}

